package com.minecraftonline.duelist.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.*;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

@CommandPermission("duelist.command")
@CommandAlias("duel")
public class DuelCommand extends BaseCommand {

    @Default
    @HelpCommand
    public static void onHelp(CommandSource source, CommandHelp help){
        source.sendMessage(Text.of(" "));
        source.sendMessage(Text.of(TextColors.RED, "\u2694[Duelist Commands]\u2694"));
        source.sendMessage(Text.of(" "));
        source.sendMessage(Text.of(TextColors.RED, "/duel request ", TextColors.GRAY, "[player1] [player2]..", TextColors.WHITE, " - Request to duel 1 or more players."));
        source.sendMessage(Text.of(TextColors.RED, "/duel request ", TextColors.GRAY, "--a [Arena] [player1] [player2]..", TextColors.WHITE, " - Request to duel 1 or more players in a specific arena."));
        source.sendMessage(Text.of(TextColors.RED, "/duel deny ", TextColors.GRAY, "[player]", TextColors.WHITE, " - Deny a duel request from another player."));
        source.sendMessage(Text.of(TextColors.RED, "/duel accept ", TextColors.GRAY, "[player]", TextColors.WHITE, " - Accept a duel request from another player."));
        source.sendMessage(Text.of(TextColors.RED, "/duel cancel", TextColors.WHITE, " - Cancel your request to duel."));
        source.sendMessage(Text.of(TextColors.RED, "/duel arenas", TextColors.WHITE, " - List different arenas to duel in."));
        source.sendMessage(Text.of(TextColors.RED, "/duel leaderboard ", TextColors.GRAY, "[player1]", TextColors.WHITE, " - See the current leaders in pvp."));
        source.sendMessage(Text.of(TextColors.RED, "/duel stats ", TextColors.GRAY, "[player1]", TextColors.WHITE, " - See yours or others duel stats."));
    }

    @Subcommand("request|rq")
    @CommandPermission("request")
    @Description("Challenge a player to a duel.")
    public void onRequest(CommandSource source) {
        if(!(source instanceof Player)) return;
    }

}
