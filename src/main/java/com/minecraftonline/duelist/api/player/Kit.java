package com.minecraftonline.duelist.api.player;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.entity.PlayerInventory;
import org.spongepowered.api.item.inventory.property.SlotIndex;

import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

public class Kit {

    private final Map<SlotIndex, ItemStack> mainSlots = new TreeMap<>();
    private final ItemStack helmet;
    private final ItemStack chestplate;
    private final ItemStack leggings;
    private final ItemStack boots;
    private final ItemStack offHand;

    public Kit(Player player){
        PlayerInventory inventory = (PlayerInventory) player.getInventory();
        Iterable<Slot> slots = inventory.getMain().slots();
        int i = 0;
        for(Slot s : slots){
            if(s.peek().isPresent()) mainSlots.put(SlotIndex.of(i), s.peek().get());
            i++;
        }
        helmet = player.getHelmet().orElseGet(ItemStack::empty);
        chestplate = player.getChestplate().orElseGet(ItemStack::empty);
        leggings = player.getLeggings().orElseGet(ItemStack::empty);
        boots = player.getBoots().orElseGet(ItemStack::empty);
        offHand = inventory.getOffhand().peek().orElseGet(ItemStack::empty);
    }

    public void set(Player player){
        player.getInventory().clear();
        PlayerInventory pi = (PlayerInventory) player.getInventory();

        for(SlotIndex si : mainSlots.keySet())
            pi.getMain().set(si, mainSlots.get(si).copy());

        player.setHelmet(helmet.copy());
        player.setChestplate(chestplate.copy());
        player.setLeggings(leggings.copy());
        player.setBoots(boots.copy());
        pi.getOffhand().set(offHand.copy());
    }
    public static Kit of(Player player){
        return new Kit(player);
    }

    public ItemStack getBoots() {
        return boots;
    }

    public ItemStack getChestplate() {
        return chestplate;
    }

    public ItemStack getHelmet() {
        return helmet;
    }

    public ItemStack getLeggings() {
        return leggings;
    }

    public ItemStack getOffHand() {
        return offHand;
    }

    public Map<SlotIndex, ItemStack> getMainSlots() {
        return mainSlots;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Kit kit = (Kit) o;
        return Objects.equals(mainSlots, kit.mainSlots) && Objects.equals(helmet, kit.helmet) && Objects.equals(chestplate, kit.chestplate) && Objects.equals(leggings, kit.leggings) && Objects.equals(boots, kit.boots) && Objects.equals(offHand, kit.offHand);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mainSlots, helmet, chestplate, leggings, boots, offHand);
    }
}
