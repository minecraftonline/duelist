package com.minecraftonline.duelist.api.player;

import org.spongepowered.api.entity.Transform;
import org.spongepowered.api.world.World;

import java.util.Objects;

public class SpawnLocation {

    private final Transform<World> transform;

    SpawnLocation(Transform<World> transform){
        this.transform = transform;
    }

    public Transform<World> getTransform(){
        return transform;
    }

    public static SpawnLocation of(Transform<World> transform) {
        return new SpawnLocation(transform);
    }
    @Override
    public boolean equals(Object object){
        if(object == this) return true;
        if(!(object instanceof SpawnLocation)) return false;
        SpawnLocation spawnLocation = (SpawnLocation) object;
        return Objects.equals(this.transform, spawnLocation.transform);
    }

    @Override
    public int hashCode(){return transform.hashCode();}

}
