package com.minecraftonline.duelist.api.player;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Transform;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.world.World;

import java.util.Objects;

public class Duelist {
    private final Transform<World> transform;

    private final Kit kit;
    private final double health;
    private final int hunger;

    public Duelist(Player player){
        this.transform = player.getTransform();
        this.health = player.health().get();
        this.hunger = player.foodLevel().get();
        this.kit = Kit.of(player);
    }

    public void restore(Player player){
        player.setTransform(this.transform);
        player.offer(Keys.HEALTH, this.health);
        player.offer(Keys.FOOD_LEVEL, this.hunger);
        kit.set(player);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Duelist duelist = (Duelist) o;
        return Double.compare(duelist.health, health) == 0 && hunger == duelist.hunger && Objects.equals(transform, duelist.transform) && Objects.equals(kit, duelist.kit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transform, kit, health, hunger);
    }
}
