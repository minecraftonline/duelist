package com.minecraftonline.duelist.api;

import com.minecraftonline.duelist.api.player.Kit;
import com.minecraftonline.duelist.api.player.SpawnLocation;

import java.util.*;

public class Arena {

    private final String name;

    private int maxPlayers;

    private final UUID world;

    private final List<SpawnLocation> spawns = new ArrayList<>();

    private SpawnLocation spectator;

    private Kit kit;

    public Arena(String name, UUID world){
        this.name = name;
        this.world = world;
    }

    public String name(){return name;}

    public void setKit(Kit kit){this.kit = kit;}

    public void setMaxPlayers(int maxPlayers) {this.maxPlayers = maxPlayers;}

    public void addSpawnPosition(SpawnLocation spawnLocation){spawns.add(spawnLocation);}

    private void setSpectator(SpawnLocation spawnLocation){this.spectator = spawnLocation;}

    @Override
    public boolean equals(Object object){
        if(object == this) return true;
        if(!(object instanceof Arena)) return false;
        Arena arena = (Arena) object;
        return Objects.equals(this.name, arena.name());
    }

    @Override
    public int hashCode(){return name.hashCode();}
}
