package com.minecraftonline.duelist;

import co.aikar.commands.SpongeCommandManager;
import com.google.inject.Inject;
import com.minecraftonline.duelist.command.DuelCommand;
import com.minecraftonline.duelist.duel.DuelManager;
import org.slf4j.Logger;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.game.state.GameStoppingServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.plugin.PluginContainer;

import java.io.File;

        @Plugin(
        id = "duelist",
        name = "Duelist",
        version = "1.0",
        description = "Allows for senseless but fun violence in the form of a duel"
)
public class DuelistPlugin {

    @Inject
    private Logger logger;

    @Inject
    private PluginContainer container;

    @Inject
    @ConfigDir(sharedRoot = false)
    private File dir;

    private static DuelistPlugin instance;
    private DuelManager manager;

    @Listener
    public void enable(GameStartedServerEvent event){
        instance = this;

        SpongeCommandManager manager = new SpongeCommandManager(this.container);
        manager.createRootCommand("duel");
        manager.registerCommand(new DuelCommand());
    }

    @Listener
    public void onDisable(GameStoppingServerEvent event){

    }

    public Logger logger(){return logger;}

    public File workingDir(){return dir;}

    public static DuelistPlugin inst(){return instance;}
            public DuelManager getDuelManager(){return manager;}
}
